---
title: Legal
comments: false
---

# Sharing is Caring

Except when stated otherwise, any work on this website is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# Privacy & Cookie

We host our website on Netlify.
We therefore refer to the [Privacy Policy of Netlify][netlify-privacy].

Netlify relies on Cloudflare as CDN.
We therefore also refer to the [Privacy Policy of Cloudflare][cloudflare].

[netlify-privacy]: https://www.netlify.com/privacy/
[cloudflare]: https://www.cloudflare.com/privacypolicy/

# Cookies

Cloudflare sets a cookie named `__cfduid`.
This cookie is used by Cloudflare to distinguish users who use a shared IP address in order to verify requests.
