---
title: Converting Onetab to Orgmode
date: 2019-08-08
---

[Better-Onetab Firefox extension](https://github.com/cnwangjie/better-onetab) is great, but it's overwhelming after a while
Not sure if it's any better than just bookmarks, but very convenient when I need to save bunch of tabs.

Eventually I need to reconcile with the rest of my bookmarking habits, so back to [OrgMode](https://emacswiki.org/emacs/OrgMode) for organizing anything.  The export function is fine, but
I use awk to convert them to orgmode, it's pretty handy for the time being.
